// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "RipplesDevGameMode.h"
#include "RipplesDevCharacter.h"

ARipplesDevGameMode::ARipplesDevGameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = ARipplesDevCharacter::StaticClass();	
}
